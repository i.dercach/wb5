<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();

  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';

    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }

  $errors = array();
  $errors['field-name'] = !empty($_COOKIE['field-name_error']);
  $errors['field-email'] = !empty($_COOKIE['field-email_error']);
  $errors['field-date'] = !empty($_COOKIE['field-date_error']);
  $errors['radio-group-1'] = !empty($_COOKIE['radio-group-1_error']);
  $errors['radio-group-2'] = !empty($_COOKIE['radio-group-2_error']);
  $errors['field-name-4'] = !empty($_COOKIE['field-name-4_error']);
  $errors['field-name-2'] = !empty($_COOKIE['field-name-2_error']);
  $errors['check-1'] = !empty($_COOKIE['check-1_error']);
  $errors['1'] = !empty($_COOKIE['1_error']);
  $errors['2'] = !empty($_COOKIE['2_error']);
  $errors['3'] = !empty($_COOKIE['3_error']);

  if ($errors['field-name']) {
    setcookie('field-name_error', '', 100000);
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if ($errors['field-email']) {
    setcookie('field-email_error', '', 100000);
    $messages[] = '<div class="error">Заполните e-mail.</div>';
  }
  if ($errors['field-date']) {
    setcookie('field-date_error', '', 100000);
    $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if ($errors['radio-group-1']) {
    setcookie('radio-group-1_error', '', 100000);
    $messages[] = '<div class="error">Выберите пол.</div>';
  }
  if ($errors['radio-group-2']) {
    setcookie('radio-group-2_error', '', 100000);
    $messages[] = '<div class="error">Выберите количество конечностей.</div>';
  }
  if ($errors['field-name-4']) {
    setcookie('field-name-4_error', '', 100000);
    $messages[] = '<div class="error">Выберите сверхспособность.</div>';
  }
  if ($errors['field-name-2']) {
    setcookie('field-name-2_error', '', 100000);
    $messages[] = '<div class="error">Заполните биографию.</div>';
  }
  if ($errors['check-1']) {
    setcookie('check-1_error', '', 100000);
    $messages[] = '<div class="error">Примите условия.</div>';
  }
  if ($errors['1']){
    setcookie('1_error', '', 100000);
    $messages[] = '<div class="error">Имя должно быть написано при помощи латинского алфавита без добавления лишних знаков</div>';
  } 
  if ($errors['2']){
    setcookie('2_error', '', 100000);
    $messages[] = '<div class="error">Формат эл почты имеет вид: example@email.com</div>';
  } 
  if ($errors['3']){
    setcookie('3_error', '', 100000);
    $messages[] = '<div class="error">Формат даты 02.02.2021</div>';
  } 

  $values = array();
  $values['field-name'] = empty($_COOKIE['field-name_value']) ? '' : $_COOKIE['field-name_value'];
  $values['field-email'] = empty($_COOKIE['field-email_value']) ? '' : $_COOKIE['field-email_value'];
  $values['field-date'] = empty($_COOKIE['field-date_value']) ? '' : $_COOKIE['field-date_value'];
  $values['radio-group-1'] = empty($_COOKIE['radio-group-1_value']) ? '' : $_COOKIE['radio-group-1_value'];
  $values['radio-group-2'] = empty($_COOKIE['radio-group-2_value']) ? '' : $_COOKIE['radio-group-2_value'];
  $values['field-name-4'] = empty($_COOKIE['field-name-4_value']) ? '' : $_COOKIE['field-name-4_value'];
  $values['field-name-2'] = empty($_COOKIE['field-name-2_value']) ? '' : $_COOKIE['field-name-2_value'];
  $values['check-1'] = empty($_COOKIE['check-1_value']) ? '' : $_COOKIE['check-1_value'];

  $flag = FALSE;
  foreach($errors as $er){
    if(!empty($er)){
      $flag = TRUE;
      break;
    }
    print($er);
  }

  if (!$flag && !empty($_COOKIE[session_name()]) &&
  session_start() && !empty($_SESSION['login'])) { 
        try {
        $user = 'u23978';
        $pass1 = '3457435';
        $db = new PDO('mysql:host=localhost;dbname=u23978', $user, $pass1, array(PDO::ATTR_PERSISTENT => true));
        $log1 = $_SESSION['login'];
        $pass24 = $_SESSION['pass'];
          $data = $db->query("SELECT * FROM form where login = '$log1' AND pass='$pass24'");  

          foreach ($data as $row) {
            $values['field-name'] = $row['name'];
            $values['field-email'] = $row['email'];
            $values['field-date'] = $row['date'];
            $values['radio-group-1'] = $row['radio1'];
            $values['radio-group-2'] = $row['radio2'];
            $values['field-name-4'] = $row['fieldID'];
            $values['field-name-2'] = $row['name2'];
            $values['check-1'] = $row['check1'];
        }
      } catch(PDOException $e) {
          echo 'Ошибка: ' . $e->getMessage();
      }
    
    printf('Вход с логином %s', $_SESSION['login']);
  }

  include('form.php');
}

else {
  $errors = FALSE;
  if (!preg_match("/^[-a-zA-Z]+$/",$_POST['field-name'])){
    setcookie('1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/",$_POST['field-email'])){
    setcookie('2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/^(\d{1,2})\.(\d{1,2})(?:\.(\d{4}))?$/",$_POST['field-date'])){
    setcookie('3_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['field-name'])) {
    setcookie('field-name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name_value', $_POST['field-name'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-email'])) {
    setcookie('field-email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-email_value', $_POST['field-email'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-date'])) {
    setcookie('field-date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-date_value', $_POST['field-date'], time() + 365 * 24 * 60 * 60);
  } 
  if (empty($_POST['radio-group-1'])) {
    setcookie('radio-group-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio-group-1_value', $_POST['radio-group-1'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['radio-group-2'])) {
    setcookie('radio-group-2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio-group-2_value', $_POST['radio-group-2'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-name-4'])) {
    setcookie('field-name-4_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name-4_value', $_POST['field-name-4'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-name-2'])) {
    setcookie('field-name-2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name-2_value', $_POST['field-name-2'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['check-1'])) {
    setcookie('check-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('check-1_value', $_POST['check-1'], time() + 365 * 24 * 60 * 60);
  }

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('1_error', '', 100000);
    setcookie('2_error', '', 100000);
    setcookie('3_error', '', 100000);
    setcookie('field-name_error', '', 100000);
    setcookie('field-email_error', '', 100000);
    setcookie('field-date_error', '', 100000);
    setcookie('radio-group-1_error', '', 100000);
    setcookie('radio-group-2_error', '', 100000);
    setcookie('field-name-4_error', '', 100000);
    setcookie('field-name-2_error', '', 100000);
    setcookie('check-1_error', '', 100000);
  }

  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    $name = $_POST['field-name'];
    $email = $_POST['field-email'];
    $date = $_POST['field-date'];
    $radio1 = $_POST['radio-group-1'];
    $radio2 = $_POST['radio-group-2'];
    $field4 = $_POST['field-name-4'];
    $name2 = $_POST['field-name-2'];
    $check = $_POST['check-1'];
    
    $user = 'u23978';
    $pass1 = '3457435';
    $db = new PDO('mysql:host=localhost;dbname=u23978', $user, $pass1, array(PDO::ATTR_PERSISTENT => true));
    $log1 = $_SESSION['login'];
    $pass24 = $_SESSION['pass'];
    
    try { 
      $stmt = $db->prepare("UPDATE form SET name='$name',email='$email',date='$date',radio1='$radio1',radio2='$radio2',fieldID='$field4',name2='$name2',check1='$check' where login = '$log1' AND pass='$pass24'");
      $stmt -> execute();
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  }
  else {
    $login = uniqid();
    $pass = rand();
    $pass2 = md5($pass);
    setcookie('login', $login);
    setcookie('pass', $pass);

  $name = $_POST['field-name'];
  $email = $_POST['field-email'];
  $date = $_POST['field-date'];
  $radio1 = $_POST['radio-group-1'];
  $radio2 = $_POST['radio-group-2'];
  $field4 = $_POST['field-name-4'];
  $name2 = $_POST['field-name-2'];
  $check = $_POST['check-1'];
  
  $user = 'u23978';
  $pass1 = '3457435';
  $db = new PDO('mysql:host=localhost;dbname=u23978', $user, $pass1, array(PDO::ATTR_PERSISTENT => true));
  
  try {
    $stmt = $db->prepare("INSERT INTO form (name,email,date,radio1,radio2,fieldID,name2,check1,hash,login,pass) VALUE (:name,:email,:date,:radio1,:radio2,:field4,:name2,:check1,:hash,:login,:pass)");
    $stmt -> execute(['name'=>$name,'email'=>$email,'date'=>$date,'radio1'=>$radio1,'radio2'=>$radio2,'field4'=>$field4,'name2'=>$name2,'check1'=>$check,'hash'=>$pass2,'login'=>$login,'pass'=>$pass]);
  }
  catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
  }
  }

  setcookie('save', '1');

  header('Location: index.php');
}
if(isset($_POST['exit'])){
  session_destroy();
  header('Location: login.php');
}

