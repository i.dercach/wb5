<?php

header('Content-Type: text/html; charset=UTF-8');

session_start();

if (!empty($_SESSION['login'])) {

  if(isset($_POST['exit'])){
    session_destroy();
    header('Location: login.php');
  } else {

  header('Location: index.php');}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Project 5</title>
  <!-- CSS only -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <!-- JS, Popper.js, and jQuery -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
    crossorigin="anonymous"></script>
  <!-- Main CSS -->
  <link rel="stylesheet" href="style.css">
</head>

<body class="text-dark">
  <div class="container-fluid p-0 w-100 overflow-hidden">
    <header class="row d-flex flex-row justify-content-center">
      <div
        class="d-flex flex-row align-items-center justify-content-around justify-content-md-start col-md-9 px-md-4 h-100">
        <img class="mr-md-2 border border-white rounded-circle" id="img" src="logo.png" alt="LOGO">
        <div class="text-body" id="name">Project 5</div>
      </div>
    </header>

    <div class="row d-flex flex-row justify-content-center h-100 p-md-5">
      <div class="d-flex col-md-9 p-0 justify-content-center">
        <div class="items d-flex flex-column align-content-center">

          <div id="form" class="align-content-center">

            <h2>Авторизация</h2>

            <form action="" method="post">
            <label>
                Логин:<br />
                <input name="login" />
              </label><br />
              <label>
                Пароль:<br />
                <input name="pass" />
              </label><br />
            <input type="submit" value="Войти" />
            <?php
            if (empty($_SESSION['login'])) {
              print('<input a href="index.php" type="submit" name="form" value="Еще не отправлял форму" />');
            }
              ?>
            </form>
          </div>

        </div>
      </div>
    </div>

  </div>
</body>

<?php
}
else {
  $user = 'u23978';
  $pass = '3457435';
  $db = new PDO('mysql:host=localhost;dbname=u23978', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

  $pass0 = $_POST['pass'];
  $log0 = $_POST['login'];
  $data = $db->query("SELECT * FROM form where login = '$log0' AND pass='$pass0'");
  $res = $data->fetchALL();

    if($res[0]['login']!=$log0 || $res[0]['pass']!=$pass0){
      echo 'Ошибка: Пользователь не существует!' ;
    } else{
      $_SESSION['login'] = $log0;
      $_SESSION['pass'] = $pass0;
      $_SESSION['uid'] = 123;

      header('Location: index.php');}
    
}
